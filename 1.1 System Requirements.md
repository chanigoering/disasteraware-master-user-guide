GETTING STARTED
===============

DisasterAWARE<sup>®</sup> is the most powerful and reliable early warning and multi-hazard monitoring platform. It is used by thousands of the most demanding government customers and humanitarian assistance organizations around the globe. It supports disaster risk reduction and best practices throughout all phases of disaster management—providing early warning, multi-hazard monitoring, modeled impact assessments, and the largest collection of scientifically verified geospatial data. DisasterAWARE powers [<u>customized software solutions</u>](https://www.pdc.org/solutions/early-warning-and-decision-support-solutions/) for multiple countries around the world, as well as our free mobile app for public use, [<u>Disaster Alert™</u>](https://disasteralert.pdc.org/disasteralert/).

This section introduces you to key system requirements and features necessary to get you started in DisasterAWARE. Some of these features are described in more detail later in this guide.

System Requirements
-------------------

### Supported browsers

DisasterAWARE is supported on the following browsers:

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-1-Media/media/image1.png" style="width:0.85417in;height:0.85417in" alt="Chrome Icon" /></td><td>Google Chrome 51+</td><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-1-Media/media/image2.png" style="width:0.86111in;height:0.86111in" alt="Mozilla Firefox icon" /></td><td>Mozilla Firefox 47+</td><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-1-Media/media/image3.png" style="width:0.86806in;height:0.86111in" alt="Apple Safari" /></td><td>Safari 9+</td></tr></tbody></table>

### Internet or mobile connection speed

For optimal performance, we strongly recommend you have broadband internet with at least 4 Mbps connection speed, or a 4G mobile data connection to use DisasterAWARE.

### Desktop monitor size

Desktop computers must have at least a 1024 x 768 resolution monitor to successfully operate the software. We recommend a 1920 x 1200 monitor to ensure the best experience.

### Add pdc.org as a trusted site

Please be sure to whitelist \*.pdc.org as a trusted site in your organization’s firewall rules.

### Configure and optimize your browser

-   **Turn on cookies.** PDC uses cookies for session management so please turn on browser cookies. Cookies are typically on by default.

-   **Allow PDC pop-ups.** Be sure to enable pop-up windows when using DisasterAWARE as many of the features and functions of the system are provided this way.

-   **Check your browser zoom settings.** Be sure your browser zoom settings are reset to 100%. If the browser window is zoomed in or out, it may result in an optimized screen view with fewer features. Press “Ctrl + 0” keys (or “Cmd + 0” on a Mac) to reset your browser’s zoom settings.