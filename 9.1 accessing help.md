TROUBLESHOOTING AND HELP
========================

Accessing help documents and links
----------------------------------

You can access a variety of resources such as videos, our Quick Start Guide, Release Notes (which contain details about known issues and/or potential bugs), and much more. While in DisasterAWARE, click the **Help** icon as shown below to view the help panel.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-9-Media/media/image1.png" style="width:2.89923in;height:5.40789in" alt="A screenshot of a cell phone Description automatically generated" />