**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 6.8.1 |** Release Date: 11 May 2021

New Features 
============

**Bookmark | Figure eraser:** Added the ability for users to delete figures from the bookmark figure inspector by clicking the Eraser icon from the panel toolbar. The eraser cursor icon has also been changed to reflect an eraser as opposed to a plus (+) symbol.

**Event Brief:** Adds an Event Brief button to open hazard specific event briefs. The feature is accessible from the Event Brief tab of the Hazard inspector panel or from the Hazards Map Tip.

**Layer status notification:** Added a notification for users to confirm when layers fail to load in the application. The notification will appear in the same box that Hazard notifications appear.

Issues Resolved
===============

**Bookmark | File Import:** Fixed an issue that ocurred when users inspected details of an imported bookmark file in which the title and description would disappear.

**Drawing tool | Images style: Updated the button representation for ‘Scale with Map Zoom’ to be less confusing to users on the feature functionality.**

**Firefox | Search box: Users will now see the X button to clear search fields within the application while using Firefox browser.**

**Media | Scanner: Fixed an issue with the media scanner location in the application that occurred when the scanner was enabled and disabled multiple times in the same session.**

**Media | Story update: Stories will update appropriately if the user moves the scanner location to inspect new articles in a different area than first scanned.**

**Offline | Hide on web: The offline feature will no longer show in web browsers but will show when logged in through the mobile application.**

**Products | Timeline: Product timeline will now update and display appropriately for user admin who change the product details.**

**Translation | Summaries and Reports: Fixed an issue in which Identify tool summaries and Area Brief Reports would not show the app translated titles for the Reports text.**

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers**: Burmese language does not translate on some animated

layers with a time scale and instead displays “NaNh.”

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Print Service | Map Tips:** Map Tips are not supported for Prints at this time.

**Print Service | Paper size:** A0 and A1 paper sizes (poster sizes) are not currently available.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.
