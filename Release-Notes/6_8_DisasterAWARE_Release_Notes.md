**RELEASE NOTES**

**DisasterAWARE<sup>®</sup> v 6.8 |** Release Date: 5 April 2021

New Features
============

**Asset tracking | Minimum buffer radius:** Users can now set the minimum buffer radius for imported assets to a value of zero (0). If the minimum buffer radius for imported assets is set to a value of zero (0), SmartAlert notifications will only be triggered when a hazard directly impacts the specific asset intersecting with a hazard, as opposed to alerting on a radius outside of the asset.

**Print Service:** Users now have the ability to generate high resolution images of active layers and custom map figures to be printed or downloaded as a PDF, PNG, or JPEG.

**Drawing | Coordinates:** All figures now have an option to show a label with figure coordinates. The coordinates displayed are the center coordinates for all figures except polylines which shows the coordinates of the final endpoint. (Does not apply to annotations.)

**Drawing | Geometry editor:** The ability to edit a figure’s location by coordinate is now available through the use of Info tab edit function.

**Drawing | Image scale:** Images added to maps may be set to maintain their original size, versus scaling with the zoom extent of the map. Under the style tab, toggle the option ‘Scale with Map Zoom’ to change the default scaling behavior.

**Drawing | Labels:** All figures now have the option to show a label with figure name and/or figure coordinates. (Does not apply to annotations.)

**Hazards | Go To link:** The ‘Go To’ link from the Hazards panel has been improved to better highlight the ability to zoom to a hazard location on the map.

**Product parent folder:** When products are being added to the system by administrators from within a parent folder, they will be stored by default in the parent folder. Administrators retain the ability to change the parent folder for any product when creating or editing products

**SmartAlert Notifications | LOGIN requirement:** SmartAlert notifications are now restricted by the presence or absence of the LOGIN role on accounts. In the absence of a LOGIN role no notifications will be sent to users.

Issues resolved
===============

**ArcGIS URL limit:** Previously, user-imported map services required strict adherence to ArcGIS requirements regarding mixed-case variation. The application will now modify recognized sources to transform the URL appropriately.

**Bookmarks | Drawing locations:** Fixed an issue affecting drawings that were modified from the drawing panel and would not be appropriately reset when the bookmark was reselected in the same session.

**Drawing tools | Placemarks:** Fixed an issue where Placemarks created with UNOCHA or other imported placemark symbology may not have retained the appropriate appearance when shared via bookmarks.

**GPX Exports:** Fixed an issue where some feature information would not correctly save and be visible in GPX Exports from the application.

**Improved layer search:** Layer search will now be usable in the set language DisasterAWARE is translating. Previously layers were only searchable in English when the app was translating to another language.

**Legend | Lines:** The legend appearance for line data has been changed to show a single horizontal line. Previously the appearance for line data was a colored border.

**Product creation:** Resolved an issue that occurred when Administrators added a hazard product inside a specified parent folder.

Deprecated
==========

**User Panel | Account:** On the Account section of the User panel, the organization and title fields have been removed.

Known Issues
============

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to [<span class="underline">http://disasteraware.pdc.org/help/How\_To\_Disable\_AdBlockers.pdf</span>](http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf)

**Burmese language on animated layers**: Burmese language does not translate on some animated

layers with a time scale and instead displays “NaNh.”

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in.

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase “I” may look like a lowercase “L.” Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension “Myanmar Font Tools” for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox | Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Safari | Logout:** If a Safari user clicks the back arrow after logging out the application will temporarily be reloaded. We recommend using the Firefox or Chrome browsers instead.

**Safari | Save to File**: The bookmark option to “Save to File” feature is currently not available for Safari users.

**SmartAlert Notifications | Stop:** Where available, when a user replies “Stop” to a text (SMS)

SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications | Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications | Assets:** Users who have their alert area set as “Global” but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive “Global” notifications, users should delete the Asset Providers and stop tracking assets.
