RELEASE NOTES
=============

**DisasterAWARE® v 6.6.3 \|** Release date: June 23, 2020

**Please Note:** All changes for 6.6.0, 6.6.1, and 6.6.2 were publicly released with version 6.6.3 and are documented as part of the 6.6.3 Release Notes below.

New Features 
------------

**Asset Tracking:** Track stationary or moving assets (such as buildings, personnel, or goods in transit), and receive alerts when a hazard poses a potential threat. Click the SmartAlert icon from the left toolbar to get started.

**Data uploads and sharing:** Import a KML file to add a layer to your private DisasterAWARE account or share it publicly using the bookmark feature. Click the Layers icon on the left toolbar and then Edit to add a layer. To turn on the layer you've imported, click the Layers icon and scroll down to the User Layers folder.

**Map Service Integration**: You can now import data layers from an external map service by clicking the URL link. To turn on the layer you've imported, click the Layers icon and scroll down to the User Layers folder.

**Media Panel:** Increase your situational awareness with the new Media hot spots panel. Click the Media icon on the left toolbar to search news by keyword or click a hot spot on the map to get news stories covering major events happening around the world.

**Multi-Language Login**: We've added multi-language support for login pages. Click the language translation dropdown menu to translate the page.

**SmartAlert Enhancements:** Sign up to receive early warning alerts via text or email for multiple types of hazards anywhere around the globe. SmartAlert allows you to set notification preferences for hazards according to type and severity, and to define multiple geographic locations for alerts.

When defining the SmartAlert preferences, you can now set all hazard types to the same alert severity clicking the column you want to set for all: warning, watch, advisory or information.

**Freehand tool added to the Identify panel:** Users can now use the freehand drawing tool to create a perimeter around a specific geographic area.

Issues resolved
---------------

**Camera refresh times:** Camera refresh times now show minutes and seconds instead of just seconds.

**Disaster Alert help links:** Disaster Alert mobile app help links, displayed in the help tab, now update to reflect the application being used when a user changes their domain from the default instance of DisasterAWARE that comes with Disaster Alert, to a different instance such as InAWARE, EMOPS, DMRS, etc.

**Layer import enhancements:** Layers that include polygon shapes with holes and items with non-contiguous geometry are now supported.

**Geocoded product style:** The time and product location no longer overlap in the product menu on geolocated hazard products.

**Hazard date filter stuck on current date**: Users can now consistently filter hazards by start and end date.

**Identify panel columns with empty values**: Users examining a summary generated in the identify panel may sort columns, even when some rows do not contain values for that column. Empty values will sort at top when the sort order is ascending.

**Initial application loading error:** Users no longer need to clear their browser cache to see hazards after a DisasterAWARE update.

**Log out of multiple tabs:** Users can now log out of DisasterAWARE in a single tab, closing all DisasterAWARE sessions that might be open in other browser tabs.

**Map service imports create duplicate folders**: When a user uploads map services, duplicate folders are no longer created in the layers tree view menu.

**Multiple map services in one session:** Users adding new layers can now add more than one map service by URL in a session.

**Repeated field names:** Imported map services from a KML file in DisasterAWARE Enterprise no longer repeat the first field when a user opens the map tooltip for that item.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Sharing bookmarks: **User imported .kml files can be shared via bookmark by copying, pasting and sharing the url, but if the bookmark is downloaded and shared as a JSON file, the layers are not visible. 

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Burmese language on animated layers**: Burmese language does not translate on some animated layers with a time scale and instead displays "NaNh."

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Firefox \| Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 \| Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 \| Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 \| Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the "Recycle Bin" parent folder.

**Internet Explorer 11 \| Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 \| InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari \| Save to File**: The bookmark option to "Save to File" feature is currently not available for Safari users.

**SmartAlert Notifications:** When a user replies "Stop" to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**Street View**: Street View is temporarily disabled.

**User Panel:** On the Account section of the User panel, the organization and title is not saved.
