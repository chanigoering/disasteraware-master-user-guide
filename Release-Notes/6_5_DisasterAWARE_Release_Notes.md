RELEASE NOTES
=============

**DisasterAWARE® v 6.5 \|** Release date: Nov. 14, 2019

New Features
------------

**Download products as PDF:** Users can now download products as a PDF. While viewing a product in a hazard panel, click on the info icon to the Download Product button.

**Layers Folder:** Empty folders will be hidden until an item is added. For example, if you do not have any layers marked as a favorite, the Favorites folder will not appear in the main layers panel.

**Line Draw Tool:** The Line tool in the Drawing panel now offers a distance attribute to measure the total distance in km or miles covered by the line. This can be found in the Label tab and enabled by toggling the Show Measurement Label button.

Issues resolved
---------------

**Log in:** Erroneous spaces that are entered at the end of a username will no longer keep a user from logging in.

**Bookmarks:** More than three bookmarks can now be added in a row and will be updated in the bookmark panel.

**Drawing tools:** Exported KML files will stay checked (enabled). When adding an image with the drawing tool, users can click anywhere on the button and not just on the button's icon.

**SmartAlert:** Users will select Done to save SmartAlert areas.

**Layers:** Catalog layers will now be translated.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Hazard Info:** The "More Information" button that provides access to the Hazard Brief report does not appear for wildfire or flood hazards.

**Firefox \| Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 \| Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 \| Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 \| Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the "Recycle Bin" parent folder.

**Internet Explorer 11 \| Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 \| InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari \| Save to File**: The bookmark option to "Save to File" feature is currently not available for Safari users.

**SmartAlert Notifications:** When a user replies "Stop" to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**Street View panning**: You cannot pan the map in Street View mode.

**User Panel:** On the Account section of the User panel, the organization and tile is not saved.
