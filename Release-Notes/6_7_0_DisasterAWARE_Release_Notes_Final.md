Release Notes 
=============

**DisasterAWARE® v 6.7.0 \|** Release Date: 14 August 2020

Issues resolved
---------------

**Bookmarks:** Fixed an issue where legacy bookmarks would not load appropriately.

**Chinese language:** Fixed an issue where language preferences when set to Taiwan Chinese (zh-TW) would revert to mainland simplified Chinese (zh-CN) when the language panel was closed.

**Feature symbology**: Feature symbols will match across views within the map. Features may have appeared in different proportions when viewed on the map and in the legend.

**Imported KML Files:** Imported KMLs will now display names using the file name property if a name tag is not specified.

**Layer folders in Recent Folder:** Fixed an issue where the layer folder would appear inside the User Layer folder if every layer inside the folder was turned on. Now, only layers will appear in the Recent folder.

**Notifications**: Fixed an issue where popup notifications are displayed with every scheduled hazard refresh.

**Street View**: Street view now displays as expected.

**Virtual Layer loading**: Fixed an issue where virtual layers that contained point and polygon features, such as CDC Travel Notices, would load asynchronously.

Deprecated
----------

**System of Measurement preference removed**: Removed the "System of Measurement" user interface from the User Preference panel.

**Offline mode**: Offline Mode removed pending further enhancements.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Burmese language on animated layers**: Burmese language does not translate on some animated layers with a time scale and instead displays "NaNh."

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase "I" may look like a lowercase "L." Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Firefox \| Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 \| Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 \| Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 \| Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the "Recycle Bin" parent folder.

**Internet Explorer 11 \| Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 \| InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari \| Save to File**: The bookmark option to "Save to File" feature is currently not available for Safari users.

**Sharing bookmarks: **User imported .kml files can be shared via bookmark by copying, pasting and sharing the url, but if the bookmark is downloaded and shared as a JSON file, the layers are not visible. 

**SmartAlert Notifications \| Stop:** When a user replies "Stop" to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications \| Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications \| Assets:** Users who have their alert area set as "Global" but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive "Global" notifications, users should delete the Asset Providers and stop tracking assets.

**User Panel:** On the Account section of the User panel, the organization and title is not saved.
