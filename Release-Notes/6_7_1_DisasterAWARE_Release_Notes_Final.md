Release Notes 
=============

**DisasterAWARE® v 6.7.1 \|** Release Date: 15 September 2020

New features 
------------

**Assets \| hyperlinks:** Property values of features that are hyperlinks will be formatted into a clickable link from within the feature inspector and map tip to open the link in a new tab.

**Hazards \| More Info:** Added a field visible on Hazard Create/Edit forms to allow Hazard Admin to directly enter/update the "More Info" button redirect.

**Hazards \| SmartAlert areas:** Added the ability for Hazard Admin to directly edit the SmartAlert areas of hazards.

**KML descriptions:** User imported KML files with inline HTML will have the descriptions shown as the embedded HTML in the feature inspector. The map tip description will be shortened to two lines for usability.

Issues resolved
---------------

**Bookmarks:** Fixed an issue where legacy bookmarks would not load appropriately. Also, fixed an issue where users who edited layers of bookmarks may have found the app in an unusable state.

**Chinese language:** Fixed an issue where language preferences when set to Taiwan Chinese (zh-TW) would revert to mainland simplified Chinese (zh-CN) when the language panel was closed.

**Drawing:** Fixed an issue where users were not able to add images when the selector was chosen from the right-side map toolbar.

**Drawing \| Eraser:** When users delete a drawing figure the map will not pan as the user moves the cursor. This affected users in Firefox and Safari browsers.

**Drawing \| Placemark:** The placemark cursor image now appears the same as other drawing cursors.

**Feature symbology**: Feature symbols will match across views within the map. Features may have appeared in different proportions when viewed on the map and in the legend.

**Hazard share:** Users who click the "Share" from the hazard Info panel will no longer have extra text copied to the clipboard when using Google Chrome.

**Identify \| Duplicates:** Fixed an issue where some features may have resulted in duplicate records when an Identify Summary was reviewed.

**Imported KML Files:** Imported KMLs will now display names using the file name property if a name tag is not specified.

**KML descriptions \| Long descriptions:** KML files with long descriptions will have the description start on a new line and use the entire panel width for reading, making long descriptions easier for users to read.

**Layer folders in Recent Folder:** Fixed an issue where the layer folder would appear inside the User Layer folder if every layer inside the folder was turned on. Now, only layers will appear in the Recent folder.

**Layer abstract:** Fixed an issue where some layers may not correctly display abstract information within the app.

**Layer flickering:** Made enhancements to improve "flickering" of Global Layers when a user panned the map.

**Layer import change:** When users import geospatial files, the application will automatically transition to the layer inspector's feature tab on successful import. This allows users to review the features of the file they just imported.

**Layers \| Favorite & Recent folder:** Fixed an issue where users who enabled cameras during their session may see 'layers' without titles in their Favorite & Recent Layer folders.

**Media:** Fixed an issue where story keywords would be cut-off the text area if the media source URL was too long.

**Street View**: Street view now displays as expected.

**Translations:** Made enhancements to improve slow translation speeds.

**Virtual Layer loading**: Fixed an issue where virtual layers that contained point and polygon features, such as CDC Travel Notices, would load asynchronously.

Known Issues
------------

Listed below are known issues and available workarounds. Thank you for your patience while we address these items.

**Area Brief:** The map displayed inside the *Infrastructure & Critical Facilities* section of Area Brief may only display the location of nuclear facilities. For the location of other types of infrastructure and critical facilities, you may still find this information in the table displayed beneath the map.

**Area Brief:** Risk & Vulnerability Assessment (RVA) layers presented may not match the legends in the report.

**Area Brief \| Circle Selector:** The Circle selector tool may not be used to create Area Brief reports.

**Browser Ad Blockers:** Supplemental information products such as Sit Reps, map graphics, and other analytical reports that are associated with a Hazard cannot be opened when an ad or pop-up blocker is running on your browser. Turn off or pause the ad blocker to open and view PDF products. For instructions, go to <http://disasteraware.pdc.org/help/How_To_Disable_AdBlockers.pdf>

**Burmese language on animated layers**: Burmese language does not translate on some animated layers with a time scale and instead displays "NaNh."

**Drawing tool disabled:** If you are unable to add drawings on the map or are experiencing problems with the drawing tools, you may be zoomed into the map too closely. Zoom out a bit and then try again.

**Fonts on Bing Roads basemap**: Some text on the Bing roads basemap will appear split into two font sizes. To correct this problem with the text, please zoom in. 

**Fonts in DisasterAWARE**: Some letters may not appear correctly if the default zoom level has changed. For example, a lowercase "I" may look like a lowercase "L." Users can verify spelling if needed by changing the zoom level of the browser and temporarily zoom in to view the text.

**Fonts in DMRS**: The appearance of English user interface elements in DMRS may not appear correctly if users install & use the Google Chrome extension "Myanmar Font Tools" for ZAWGYI encoding standards. This issue only affects users on ZAWGYI computers.

**Firefox \| Hazards:** Administrators may experience problems manually adding Hazards to the system if using a Firefox browser with private browsing enabled. If privacy is enabled, administrators who want to manually expire a Hazard, may need to refresh their browser to see the hazard expire from the map.

**Internet Explorer 11 \| Stability**: IE11 users may experience slowdowns when moving the map, opening menus, and opening products. Users may also experience intermittent program crashes.

**Internet Explorer 11 \| Bookmarks:** Bookmark edits may not appear in IE 11 until the cache is cleared.

**Internet Explorer 11 \| Attachments:** IE 11 administrators cannot change a product that has been uploaded (jpg, pdf or txt, for example). If a user cannot switch to a different browser or create a new product with the new file, hide the original product by adding it to the "Recycle Bin" parent folder.

**Internet Explorer 11 \| Drawing tools:** In IE 11, the Free Line and Free Polygon tools may shift away from their intended location (cursor position) while being drawn.

**Internet Explorer 11 \| InPrivate browsing**: The product list preview will not appear with private browsing enabled (IE InPrivate browsing mode).

**Safari \| Save to File**: The bookmark option to "Save to File" feature is currently not available for Safari users.

**Sharing bookmarks: **User imported .kml files can be shared via bookmark by copying, pasting and sharing the url, but if the bookmark is downloaded and shared as a JSON file, the layers are not visible. 

**SmartAlert Notifications \| Stop:** When a user replies "Stop" to a text (SMS) SmartAlert, the response they receive is in English, regardless of which language is selected.

**SmartAlert Notifications \| Chinese:** Users who have their language preferences set to Chinese will receive notifications in English. Chinese language options (zh-CN and zh-TW) are not supported for SmartAlert notifications.

**SmartAlert Notifications \| Assets:** Users who have their alert area set as "Global" but who have active Asset Providers may only receive notifications for hazards that intersect their active tracked Assets. To receive "Global" notifications, users should delete the Asset Providers and stop tracking assets.

**User Panel:** On the Account section of the User panel, the organization and title is not saved.
