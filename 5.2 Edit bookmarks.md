Edit bookmarks
--------------

It’s often helpful to be able to edit a bookmark you’ve shared with new updates from the field. For instance, you may want to add additional drawings that delineate assessed structures, or a change in status (e.g. planned assessment areas, completed assessments).

### Change bookmark extent

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image6.png" style="width:0.27778in;height:0.27778in" alt="A close up of a logo Description automatically generated" /></td><td>While in the <strong>Bookmarks</strong> panel, click the <strong>Info</strong> icon next to the name of a bookmark you want to update. Zoom or re-center the map and then select the <strong>Update Bookmark Extent</strong> button. Your bookmark will now open to the updated extent when revisited.</td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image7.png" style="width:6.65789in;height:2.26519in" alt="A screenshot of a social media post with text and people in the background Description automatically generated" />

### Add figures to an existing bookmark

You can add drawings, photos, text annotations and other figures to a bookmark you’ve already created. Click the **Info** icon next to the bookmark you want to edit. While in the **Bookmarks** panel, click the **Figures** tab.

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image8.png" style="width:0.225in;height:0.225in" /></td><td>Once in the <strong>Figures</strong> panel, click the <strong>Edit</strong> icon. Next, click the <strong>Drawing</strong> icon to add a figure. You can organize figures into folders by clicking the folder icon, and then dragging and dropping the figures into the folder.</td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image10.png" style="width:6.45614in;height:1.27778in" alt="A screenshot of a cell phone Description automatically generated" />

### Add layers to an existing bookmark

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image8.png" style="width:0.225in;height:0.225in" /></td><td>You can add or change <strong>Layers</strong> in an existing bookmark. Click the <strong>Info</strong> icon next to the bookmark you want to edit. While in the <strong>Bookmarks</strong> panel, click the <strong>Layers</strong> tab and then the <strong>Edit</strong> icon as shown below.</td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image11.png" style="width:2.38158in;height:1.79363in" alt="A screenshot of a cell phone Description automatically generated" />

###  View and manage your bookmarks

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image1.png" style="width:0.28472in;height:0.28472in" /></td><td>To view your list of bookmarks, edit the name of a bookmark, delete or export a bookmark, or to locate the URL of a bookmark you’ve already created, click the <strong>Bookmark</strong> icon on the left toolbar to view your list of bookmarks.</td></tr><tr class="even"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image6.png" style="width:0.27778in;height:0.27778in" alt="A close up of a logo Description automatically generated" /></td><td>Click the <strong>Info</strong> icon to the right of the bookmark name in your bookmarks list to copy/paste the bookmark link. This icon also provides helpful information about the map extend, layers activated, and when the bookmark was created.</td></tr></tbody></table>

Follow prompts below to delete, duplicate, or share a bookmark as a JSON file that can be imported into another person’s account.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image12.png" style="width:6.65217in;height:4.05334in" alt="A screenshot of a computer Description automatically generated" />

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-5-Media/media/image13.png" style="width:0.22329in;height:0.22917in" /></td><td>Use the <strong>Search</strong> option in the <strong>Bookmarks</strong> panel to locate bookmarks by keyword.</td></tr></tbody></table>