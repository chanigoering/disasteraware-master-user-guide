ADVANCED MONITORING TOOLS AND CAPABILITIES
==========================================

Track the safety of your assets
-------------------------------

Track your stationary or moving assets (such as buildings, personnel, or goods in transit), and receive alerts when a hazard poses a threat.

### Import asset to be tracked

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image1.png" style="width:0.28472in;height:0.28472in" /></td><td><p>To setup asset tracking, click the <strong>SmartAlert</strong> icon on the left toolbar and then click the <strong>Assets</strong> link.</p><p>Once the <strong>Assets</strong> panel opens, click the <strong>Providers</strong> tab.</p></td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image3.png" style="width:4.61594in;height:1.74785in" alt="A screenshot of a cell phone Description automatically generated" />

While in the **Providers** tab, click the **Create Asset Provider** button. You will need to add a data source for the assets you want to monitor.

Enter a meaningful name for the asset data layer you want to import and click the **Source** link. Next, click the **Edit** icon to begin the process of adding an asset layer to the system.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image4.png" style="width:5.18051in;height:2.74638in" alt="A screenshot of a cell phone Description automatically generated" />

Now click the **Plus** (+) symbol and add a web service URL for the assets layer you wish to import. If required, you can also add credentials needed to access the service by clicking the **Authorization** link.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image5.png" style="width:6.3317in;height:2.375in" alt="A screenshot of a cell phone Description automatically generated" />

To add authorization credentials, click the **Edit** icon, then the **Plus** (+) symbol, and then enter the layer authorization details shown below. Authorizations will remain in the list for future use, should you wish to import other asset sources that utilize the same authorization. You can remove authorizations at any time.

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image6.png" style="width:6.87414in;height:2.43056in" alt="A screenshot of a cell phone Description automatically generated" />

### View imported asset layers

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image7.png" style="width:0.26389in;height:0.26389in" alt="Layers Icon from toolbar" /></td><td>Click the <strong>Layers</strong> icon in the left toolbar to view your list of layers.</td></tr><tr class="even"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image8.png" style="width:0.26389in;height:0.20833in" alt="A picture containing drawing Description automatically generated" /></td><td>The asset layer(s) you’ve added will appear under the <strong>User Layers</strong> folder along with any other data layers you’ve previously imported.</td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image9.png" style="width:6.29167in;height:3.82057in" alt="A screen shot of a computer Description automatically generated" />

### Delete asset layers and authorization credentials

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image7.png" style="width:0.26812in;height:0.26812in" alt="Layers Icon from toolbar" /></td><td>While in the Layers panel, select the <strong>Edit</strong> button. Expand the <strong>User Layers</strong> folder and select the <strong>Delete</strong> button shown below for the layer you wish to remove.</td></tr></tbody></table>

<img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image10.png" style="width:1.9625in;height:2.80556in" alt="A screenshot of a cell phone Description automatically generated" />

<table><tbody><tr class="odd"><td><img src="https://bitbucket.org/pacificdisastercenter/disasteraware-master-user-guide/raw/master/Chapter-7-Media/media/image11.png" style="width:0.41304in;height:0.34783in" alt="A picture containing drawing Description automatically generated" /></td><td>You cannot delete authorization credentials until asset layers utilizing that authorization have been removed from your list of Asset Providers. Click the <strong>SmartAlert</strong> icon on the left toolbar, then <strong>Assets &gt; Sources</strong> and select the credential you wish to delete. Next click the <strong>Edit</strong> icon and the <strong>Delete</strong> (x) icon.</td></tr></tbody></table>